export function formatCurrency(price: string | undefined) {
  const convertToNumber = Number(price)

  const priceFormatted = convertToNumber?.toLocaleString('pt-BR', {
    style: 'currency',
    currency: 'BRL'
  })

  return priceFormatted
}
