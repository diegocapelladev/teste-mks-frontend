import CartButton from 'components/CartButton'
import { Container } from 'components/Container'
import Logo from 'components/Logo'
import * as S from './styles'

const Navbar = () => (
  <S.Navbar>
    <Container>
      <S.Wrapper>
        <Logo />
        <CartButton />
      </S.Wrapper>
    </Container>
  </S.Navbar>
)

export default Navbar
