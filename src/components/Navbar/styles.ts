import styled, { css } from 'styled-components'

export const Navbar = styled.nav`
  ${({ theme }) => css`
    background-color: ${theme.colors.blue};
    color: ${theme.colors.white};
  `}
`
export const Wrapper = styled.div`
  display: flex;
  align-items: center;
  justify-content: space-between;
  height: 10rem;
`
