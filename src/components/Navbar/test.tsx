import { screen } from '@testing-library/react'
import { renderWithTheme } from 'types/helpers'

import Navbar from '.'

describe('<Navbar />', () => {
  it('should render the heading', () => {
    renderWithTheme(<Navbar />)

    expect(screen.getByRole('heading', { name: /MKS/i })).toBeInTheDocument()
    expect(
      screen.getByRole('heading', { name: /Sistemas/i })
    ).toBeInTheDocument()
    expect(screen.getByLabelText(/Cart items/i)).toBeInTheDocument()
  })
})
