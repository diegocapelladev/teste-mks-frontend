import { screen } from '@testing-library/react'
import { renderWithTheme } from 'types/helpers'

import Price from '.'

describe('<Price />', () => {
  it('should render price in label', () => {
    renderWithTheme(<Price price={'399'} />)

    expect(screen.getByText('R$ 399,00'))
  })
})
