import { formatCurrency } from 'utils/formatCurrency'
import * as S from './styles'

export type PriceProps = {
  price?: string
}

const Price = ({ price = '0' }: PriceProps) => {
  return <S.Wrapper>{formatCurrency(price)}</S.Wrapper>
}

export default Price
