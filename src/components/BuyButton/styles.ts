import styled, { css } from 'styled-components'

export const Wrapper = styled.button`
  ${({ theme }) => css`
    display: flex;
    align-items: center;
    justify-content: center;
    background-color: ${theme.colors.blue};
    color: ${theme.colors.white};
    border: none;
    width: 100%;
    padding: 0.8rem 0;
    cursor: pointer;

    span {
      margin-left: 0.8rem;
      font-size: ${theme.font.sizes.medium};
      font-weight: 600;
      text-transform: uppercase;
      align-self: flex-end;
    }
  `}
`
