import { screen } from '@testing-library/react'
import { renderWithTheme } from 'types/helpers'

import BuyButton from '.'

describe('<BuyButton />', () => {
  it('should render the button', () => {
    renderWithTheme(<BuyButton id={0} />)

    expect(screen.getByRole('button', { name: /comprar/i })).toBeInTheDocument()
    expect(screen.getByText(/comprar/i)).toBeInTheDocument()
  })
})
