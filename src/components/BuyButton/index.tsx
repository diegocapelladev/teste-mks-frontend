import { useShoppingCart } from 'context/ShoppingCartContext'
import { FiShoppingBag } from 'react-icons/fi'

import * as S from './styles'

export type BuyButtonProps = {
  id: number
}

const BuyButton = ({ id }: BuyButtonProps) => {
  const { increaseCartQuantity } = useShoppingCart()

  return (
    <S.Wrapper onClick={() => increaseCartQuantity(id)}>
      <FiShoppingBag size={20} />
      <span>Comprar</span>
    </S.Wrapper>
  )
}

export default BuyButton
