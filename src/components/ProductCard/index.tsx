import BuyButton from 'components/BuyButton'
import Price from 'components/Price'
import * as S from './styles'

export type ProductCardProps = {
  id: number
  photo: string
  name: string
  price: string
  description: string
}

const ProductCard = ({
  id,
  photo,
  name,
  price,
  description
}: ProductCardProps) => {
  return (
    <S.Card>
      <S.BoxImage>
        <S.CardImage src={photo} alt={name} />
      </S.BoxImage>
      <S.CardContent>
        <S.Box>
          <S.Title>{name}</S.Title>
          <Price price={price} />
        </S.Box>
        <S.Description>{description}</S.Description>
      </S.CardContent>
      <BuyButton id={id} />
    </S.Card>
  )
}
export default ProductCard
