import { screen } from '@testing-library/react'
import { renderWithTheme } from 'types/helpers'

import ProductCard from '.'

const props = {
  id: 0,
  name: 'Headset Cloud Stinger',
  description:
    'O HyperX Cloud Stinger™ é o headset ideal para jogadores que procuram leveza e conforto, qualidade de som superior e maior praticidade.',
  photo:
    'https://mks-sistemas.nyc3.digitaloceanspaces.com/products/hyperxcloudstinger.webp',
  price: '600.00'
}

describe('<ProductCard />', () => {
  it('should render correctly', () => {
    renderWithTheme(<ProductCard {...props} />)

    expect(
      screen.getByRole('heading', { name: props.name })
    ).toBeInTheDocument()

    expect(screen.getByRole('img', { name: props.name })).toHaveAttribute(
      'src',
      props.photo
    )
  })

  it('should render price in label', () => {
    renderWithTheme(<ProductCard {...props} />)

    expect(screen.getByText('R$ 600,00')).toBeInTheDocument
  })
})
