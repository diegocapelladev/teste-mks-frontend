import styled, { css } from 'styled-components'

export const Wrapper = styled.footer`
  ${({ theme }) => css`
    width: 100%;
    display: flex;
    align-items: center;
    justify-content: center;
    align-self: flex-end;
    background-color: ${theme.colors.lightGray};
    height: 3.4rem;
  `}
`
export const Copy = styled.p``
