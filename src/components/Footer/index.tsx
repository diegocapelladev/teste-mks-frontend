import * as S from './styles'

const Footer = () => (
  <S.Wrapper>
    <S.Copy>DC Sistemas &copy; Todos os direitos reservados</S.Copy>
  </S.Wrapper>
)

export default Footer
