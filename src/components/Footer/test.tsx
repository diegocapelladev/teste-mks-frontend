import { screen } from '@testing-library/react'
import { renderWithTheme } from 'types/helpers'

import Footer from '.'

describe('<Footer />', () => {
  it('should render the footer', () => {
    renderWithTheme(<Footer />)

    expect(
      screen.getByText('MSK Sistemas © Todos os direitos reservados')
    ).toBeInTheDocument()
  })
})
