import styled, { css } from 'styled-components'
import media from 'styled-media-query'

export const Wrapper = styled.div`
  ${({ theme }) => css`
    z-index: 999;
    width: 46.8rem;
    height: 100%;
    background-color: ${theme.colors.blue};
    position: fixed;
    top: 0;
    right: 0;
    box-shadow: rgba(0, 0, 0, 0.3) 0px 0px 12px;
    animation: showSidebar .4s;
  `}

  ${media.lessThan('medium')`
    width: 90%;
    height: 100%;
  `}

  @keyframes showSidebar {
    from {
      opacity: 0;
      width: 0;
    }
    to {
      opacity: 1;
      width: 46.8rem;
    }
  }
`

export const Container = styled.div`
  height: 100%;
  display: flex;
  flex-direction: column;
  justify-content: space-between;
`

export const Content = styled.div`
  height: 100%;
  display: flex;
  flex-direction: column;
  gap: 2rem;
  overflow: hidden;
  position: relative;
  padding: 4rem 2rem;

  ${media.lessThan('medium')`
    padding: 2rem;
  `}
`

export const Box = styled.div`
  display: flex;
  flex-direction: row;
  align-items: flex-start;
  justify-content: space-between;
`

export const Title = styled.h1`
   ${({ theme }) => css`
    width: 50%;
    color: ${theme.colors.white};
    font-size: 2.7rem;
    font-weight: 700;
  `}

  ${media.lessThan('medium')`
    ${({ theme }) => css`
      font-size: ${theme.font.sizes.xlarge};
    `}
  `}
`

export const Total = styled.h2`
  ${({ theme }) => css`
    color: ${theme.colors.white};
  `}
`

export const TotalPrice = styled.span`
  ${({ theme }) => css`
    color: ${theme.colors.white};
    font-size: ${theme.font.sizes.xlarge};
    font-weight: bold;
  `}
`

export const CloseButton = styled.button`
  ${({ theme }) => css`
    background-color: ${theme.colors.black};
    color: ${theme.colors.white};
    font-size: ${theme.font.sizes.xlarge};
    border: none;
    padding: 1rem 1.5rem;
    border-radius: 50%;
    cursor: pointer;
  `}
`

export const CheckoutButton = styled.button`
  ${({ theme }) => css`
    width: 100%;
    background-color: ${theme.colors.black};
    color: ${theme.colors.white};
    font-size: ${theme.font.sizes.xxlarge};
    font-weight: 700;
    border: none;
    padding: 2rem 0;
    cursor: pointer;
  `}

  ${media.lessThan('medium')`
    ${({ theme }) => css`
      font-size: ${theme.font.sizes.xlarge};
    `}
  `}
`
