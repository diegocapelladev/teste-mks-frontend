import CartItem from 'components/CartItem'
import { useShoppingCart } from 'context/ShoppingCartContext'
import { useState } from 'react'

import * as S from './styles'

const Cart = () => {
  const { closeCart, cartItems, getData } = useShoppingCart()
  const [total, setTotal] = useState('0,00')

  const price = async () => {
    const id = await cartItems.map((p) => {
      const prod = getData(p.id)
      const price = Number(prod?.price) * p.quantity
      return price
    })
    const total = id.reduce((total, i) => {
      return total + i
    })

    const priceFormatted = total.toLocaleString('pt-BR', {
      style: 'currency',
      currency: 'BRL'
    })
    setTotal(priceFormatted)
  }
  price()

  return (
    <S.Wrapper>
      <S.Container>
        <S.Content>
          <S.Box>
            <S.Title>Carrinho de compras</S.Title>
            <S.CloseButton onClick={closeCart}>X</S.CloseButton>
          </S.Box>

          {cartItems.map((item) => (
            <CartItem key={item.id} {...item} />
          ))}
        </S.Content>

        <S.Box style={{ alignItems: 'center', padding: '3rem 2rem' }}>
          <S.Total>Total:</S.Total>
          <S.TotalPrice>{total}</S.TotalPrice>
        </S.Box>

        <S.CheckoutButton>Finalizar Compra</S.CheckoutButton>
      </S.Container>
    </S.Wrapper>
  )
}

export default Cart
