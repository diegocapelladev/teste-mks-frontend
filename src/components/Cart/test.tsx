import { screen } from '@testing-library/react'
import { renderWithTheme } from 'types/helpers'

import Cart from '.'

describe('<Cart />', () => {
  it('should render the heading', () => {
    renderWithTheme(<Cart />)

    expect(screen.getByRole('heading', { name: /Cart/i })).toBeInTheDocument()
  })
})
