import styled, { css } from 'styled-components'
import media from 'styled-media-query'

export const Wrapper = styled.div`
  ${({ theme }) => css`
    background-color: ${theme.colors.white};
    border-radius: 0.8rem;
    position: relative;
  `}
`

export const Container = styled.div`
  display: flex;
  align-items: center;
  justify-content: space-between;
  gap: 1rem;
  padding: 1.5rem;
`
export const Image = styled.img`
  max-width: 8rem;
  max-height: 8rem;

  ${media.lessThan('medium')`
    max-width: 5rem;
    max-height: 5rem;
  `}
`

export const Title = styled.h2`
  ${({ theme }) => css`
    color: ${theme.colors.textDark};
    font-size: ${theme.font.sizes.small};
    font-weight: 400;
  `}
`

export const Quantity = styled.div`
  ${({ theme }) => css`
    display: flex;
    flex-direction: column;
    min-width: 7rem;

    p {
      font-size: ${theme.font.sizes.xsmall};
    }

    span {
      &::before {
        content: '|';
        color: #BFBFBF;
        margin-right: 0.5rem;
      }

      &::after {
        content: '|';
        color: #BFBFBF;
        margin-left: 0.5rem;
      }
    }
  `}
`

export const QuantityContainer = styled.div`
  ${({ theme }) => css`
    display: flex;
    align-items: center;
    justify-content: space-between;
    border: 0.1rem solid #BFBFBF;
    border-radius: ${theme.border.radius};
    padding: 0.5rem 0;
  `}
`

export const QuantityButton = styled.button`
  background: transparent;
  border: none;
  cursor: pointer;
  padding: 0 0.5rem;
`

export const Price = styled.span`
  font-weight: bold;
  width: fit-content;
  white-space: nowrap;
`

export const RemoveButton = styled.button`
  ${({ theme }) => css`
    background-color: ${theme.colors.black};
    color: ${theme.colors.white};
    padding: 0.3rem 0.6rem;
    border-radius: 50%;
    border: none;
    cursor: pointer;
    position: absolute;
    top: -0.5rem;
    right: -0.5rem;
  `}
`
