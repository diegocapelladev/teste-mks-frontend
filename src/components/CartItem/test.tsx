import { screen } from '@testing-library/react'
import { renderWithTheme } from 'types/helpers'

import CartItem from '.'

describe('<CartItem />', () => {
  it('should render the heading', () => {
    renderWithTheme(<CartItem id={0} quantity={0} />)

    expect(
      screen.getByRole('heading', { name: /CartItem/i })
    ).toBeInTheDocument()
  })
})
