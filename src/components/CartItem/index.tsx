/* eslint-disable @typescript-eslint/no-non-null-assertion */
import { useShoppingCart } from 'context/ShoppingCartContext'
import { formatCurrency } from 'utils/formatCurrency'

import * as S from './styles'

export type CardItemProps = {
  id: number
  quantity: number
}

const CartItem = ({ id, quantity }: CardItemProps) => {
  const {
    decreaseCartQuantity,
    increaseCartQuantity,
    removeFromCart,
    getData
  } = useShoppingCart()
  const data = getData(id)

  return (
    <S.Wrapper>
      <S.RemoveButton onClick={() => removeFromCart(id)}>X</S.RemoveButton>
      <S.Container>
        <S.Image src={data?.photo} alt={data?.name} />
        <S.Title>{data?.name}</S.Title>
        <S.Quantity>
          <p>Quant</p>
          <S.QuantityContainer>
            <S.QuantityButton onClick={() => decreaseCartQuantity(id)}>
              -
            </S.QuantityButton>
            <span>{quantity}</span>
            <S.QuantityButton onClick={() => increaseCartQuantity(id)}>
              +
            </S.QuantityButton>
          </S.QuantityContainer>
        </S.Quantity>
        <S.Price>{formatCurrency(data?.price)}</S.Price>
      </S.Container>
    </S.Wrapper>
  )
}

export default CartItem
