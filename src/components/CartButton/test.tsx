import { screen } from '@testing-library/react'
import { renderWithTheme } from 'types/helpers'

import CartButton from '.'

describe('<CartButton />', () => {
  it('should render the cart', () => {
    renderWithTheme(<CartButton />)

    expect(screen.getByLabelText(/Cart items/i)).toBeInTheDocument()
  })
})
