import { useShoppingCart } from 'context/ShoppingCartContext'
import { BsCart4 } from 'react-icons/bs'
import * as S from './styles'

const CartButton = () => {
  const { openCart, cartQuantity } = useShoppingCart()

  return (
    <S.Wrapper onClick={openCart}>
      <BsCart4 size={25} aria-label="Cart" />
      <S.Badge aria-label="Cart items">{cartQuantity}</S.Badge>
    </S.Wrapper>
  )
}

export default CartButton
