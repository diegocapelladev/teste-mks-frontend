import styled, { css } from 'styled-components'

export const Wrapper = styled.button`
  ${({ theme }) => css`
    display: flex;
    align-items: center;
    justify-content: center;
    gap: 2rem;
    height: 4,5rem;
    width: 9rem;
    background-color: ${theme.colors.white};
    color: ${theme.colors.textDark};
    border-radius: ${theme.border.radius};
    padding: 0.875rem;
    border: none;
    cursor: pointer;
    
  `}
`

export const Badge = styled.span`
  ${({ theme }) => css`
    align-self: end;
    font-size: ${theme.font.sizes.large};
    font-weight: ${theme.font.bold};
  `}
`
