import { screen } from '@testing-library/react'
import { renderWithTheme } from 'types/helpers'

import Logo from '.'

describe('<Logo />', () => {
  it('should render the heading', () => {
    renderWithTheme(<Logo />)

    expect(screen.getByRole('heading', { name: /MKS/i })).toBeInTheDocument()
    expect(
      screen.getByRole('heading', { name: /Sistemas/i })
    ).toBeInTheDocument()
  })
})
