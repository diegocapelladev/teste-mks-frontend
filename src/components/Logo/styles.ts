import styled, { css } from 'styled-components'

export const Wrapper = styled.div`
  ${({ theme }) => css` 
    display: flex;
    flex-direction: row;
    align-items: end;
    gap: 1rem;
    font-size: ${theme.font.sizes.xxlarge};

    h3 {
      font-weight: ${theme.font.normal};
      padding-bottom: 0.5rem;
    }
  `}
`
