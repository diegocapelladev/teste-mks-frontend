import * as S from './styles'

const Logo = () => (
  <S.Wrapper>
    <h1>DC</h1>
    <h3>Sistemas</h3>
  </S.Wrapper>
)

export default Logo
