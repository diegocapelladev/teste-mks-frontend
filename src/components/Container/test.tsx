import theme from 'styles/theme'
import { renderWithTheme } from 'types/helpers'

import { Container } from '.'

describe('<Container />', () => {
  it('should render the heading', () => {
    const { container } = renderWithTheme(
      <Container>
        <span>Teste Container</span>
      </Container>
    )

    expect(container.firstChild).toHaveStyleRule(
      'max-width',
      theme.grid.container
    )

    expect(container.firstChild).toMatchInlineSnapshot(`
      <div
        class="sc-bczRLJ DsFsJ"
      >
        <span>
          Teste Container
        </span>
      </div>
    `)
  })
})
