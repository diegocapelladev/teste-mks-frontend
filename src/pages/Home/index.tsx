import { Container } from 'components/Container'
import ProductCard from 'components/ProductCard'
import * as S from './styles'

import mock from 'data/mock.json'

const Home = () => {
  return (
    <>
      <Container>
        <S.Wrapper>
          {mock.map((product) => (
            <ProductCard
              key={product.id}
              id={product.id}
              name={product.name}
              photo={product.photo}
              description={product.description}
              price={product.price}
            />
          ))}
        </S.Wrapper>
      </Container>
    </>
  )
}

export default Home
