import styled from 'styled-components'
import media from 'styled-media-query'

export const Wrapper = styled.main`
  margin: 15rem auto;
  display: grid;
  grid-template-columns: repeat(4, 1fr);
  gap: 3.2rem 2.2rem;

  ${media.lessThan('large')`
    grid-template-columns: repeat(2, 1fr);
  `}

  ${media.lessThan('medium')`
    grid-template-columns: repeat(1, 1fr);
  `}
`
