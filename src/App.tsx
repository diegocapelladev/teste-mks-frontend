import { Route, Routes } from 'react-router-dom'

import Home from 'pages/Home'
import Footer from 'components/Footer'
import Navbar from 'components/Navbar'
import { ShoppingCartProvider } from 'context/ShoppingCartContext'

const App = () => {
  return (
    <>
      <ShoppingCartProvider>
        <Navbar />
        <Routes>
          <Route path="/" element={<Home />} />
        </Routes>
        <Footer />
      </ShoppingCartProvider>
    </>
  )
}

export default App
